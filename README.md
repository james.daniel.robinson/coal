# COAL

Collaboratively Organised Abuse List

This is a global abuse list that apps can make use of to ban/restrict or highlight users that have been identified
as abusers of the Blurt reward pool, with infringements such as farming accounts, copy-paste repetitive content, fake accounts, 
plagiarism without citing source etc.   

Please refer to coal.md for the curated abuse list. Please submit Pull Requests for additions or appeals.

## Key
+ CFARM - Comment Farming, voting own low value comments frequently 
+ CPC - Copy-Paste-Content
+ FAKE - Fake Account
+ FARM - Farming Rewards, using multiple accounts and/or low value posts
+ OTHER - Uncategorised, leave explanation in the note
+ PLG - Plagiarism

If multiple reasons use comma delimiter, ie. CPC, PLG
